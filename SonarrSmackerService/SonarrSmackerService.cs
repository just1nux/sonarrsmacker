﻿using SonarrSmacker;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SonarrSmackerService
{
	public partial class SonarrSmackerService : ServiceBase
	{
		EpisodeManager epManager;

		public SonarrSmackerService()
		{
			InitializeComponent();
			epManager = new EpisodeManager();
		}

		protected override void OnStart(string[] args)
		{
			epManager.StartUp();
		}

		protected override void OnStop()
		{
			epManager.ShutDown();
		}
	}
}
