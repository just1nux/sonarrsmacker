﻿using SonarrSmacker.SonarrData;
using SonarrSmacker.ServiceData;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;

namespace SonarrSmacker
{
	public class SonarrRetriever
	{
		private static SonarrRetriever sr = null;
		private SonarrData.Series[] seriesList = null;

		private SonarrRetriever(){}

		public static SonarrRetriever getSonarrRetriever()
		{
			if (sr == null)
				sr = new SonarrRetriever();
			return sr;
		}

		public static WantedMissing RetreiveWantedMissing() {
			string SonarrURL = DataManager.GetServiceList().setting.SonarrURL;
			string SonarrToken = DataManager.GetServiceList().setting.SonarrToken;
			SonarrRetriever sr = getSonarrRetriever();
			WebRequest request = WebRequest.Create($"{SonarrURL}/api/wanted/missing?pagesize=100");
			request.Headers.Add($"X-Api-Key:{SonarrToken}");
			WebResponse response = request.GetResponse();
			Stream dataStream = response.GetResponseStream();

			WantedMissing wm = null;
			DataContractJsonSerializer inSerializer = new DataContractJsonSerializer(typeof(WantedMissing));

			if (dataStream != null)
				wm = inSerializer.ReadObject(dataStream) as WantedMissing;

			response.Close();
			return wm;
		}

		public static void RescanSeries(int seriesId)
		{
			SonarrRetriever sr = getSonarrRetriever();

			string postData = @"{ ""name"": ""RescanSeries"",""SeriesId"": " + seriesId + @" }";
			string SonarrURL = DataManager.GetServiceList().setting.SonarrURL;
			string SonarrToken = DataManager.GetServiceList().setting.SonarrToken;

			WebRequest request = WebRequest.Create($"{SonarrURL}/api/command");
			request.Method = "POST";
			request.Headers.Add($"X-Api-Key:{SonarrToken}");
			request.ContentType = "application/x-www-form-urlencoded";

			byte[] byteArray = Encoding.UTF8.GetBytes(postData);
			request.ContentLength = byteArray.Length;
			Stream dataStream = request.GetRequestStream();
			dataStream.Write(byteArray, 0, byteArray.Length);
			dataStream.Close();

			WebResponse response = request.GetResponse();
			dataStream = response.GetResponseStream();
			StreamReader reader = new StreamReader(dataStream);
			string responseFromServer = reader.ReadToEnd();
			reader.Close();
			dataStream.Close();
			response.Close();
		}

		public static int LookupSeriesId(string seriesName)
		{
			int seriesId = -1;
			SonarrRetriever sr = getSonarrRetriever();
			string SonarrURL = DataManager.GetServiceList().setting.SonarrURL;
			string SonarrToken = DataManager.GetServiceList().setting.SonarrToken;

			if (sr.seriesList == null)
			{
				WebRequest request = WebRequest.Create($"{SonarrURL}/api/series");
				request.Headers.Add($"X-Api-Key:{SonarrToken}");
				WebResponse response = request.GetResponse();
				Stream dataStream = response.GetResponseStream();

				DataContractJsonSerializer inSerializer = new DataContractJsonSerializer(typeof(SonarrData.Series[]));

				if (dataStream != null)
					sr.seriesList = inSerializer.ReadObject(dataStream) as SonarrData.Series[];

				response.Close();
			}
			
			seriesId = sr.FindSeries(seriesName, sr.seriesList);
			return seriesId;
		}

		private int FindSeries(string seriesName, SonarrData.Series[] seriesSet)
		{
			int SeriesId = -1;

			foreach (SonarrData.Series series in seriesSet)
			{
				if (series.title.ToLower() == seriesName.ToLower())
					SeriesId = series.id;
			}
			if (SeriesId == -1)
			{
				Debug.WriteLine($"---SeriesId not found for '{seriesName}'---");
			}
			return SeriesId;
		}
	}
}