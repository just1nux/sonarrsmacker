﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SonarrSmacker.ServiceWindows
{
    /// <summary>
    /// Interaction logic for ConfirmRemoveService.xaml
    /// </summary>
    public partial class ConfirmRemoveService : Window
    {
        public int result = 0;

        public ConfirmRemoveService()
        {
            InitializeComponent();
        }

        public void Click_Cancel(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public void Click_OK(object sender, RoutedEventArgs e)
        {
            result = 1;
            Close();
        }
    }
}
