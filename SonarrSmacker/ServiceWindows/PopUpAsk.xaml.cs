﻿using SonarrSmacker.ServiceData;
using System.Collections.ObjectModel;
using System.Windows;

namespace SonarrSmacker
{
	public partial class PopUpAsk : Window
	{
        public Service service { get; set; }

		public string ServiceName
		{
			get
			{
				if (ServiceNameTextBox == null) return string.Empty;
				    return ServiceNameTextBox.Text;
			}
		}

        public string ImageName
        {
            get
            {
                if (ImageNameTextBox == null) return string.Empty;
                return ImageNameTextBox.Text;
            }
        }

        public PopUpAsk()
		{
			InitializeComponent();
		}

        public void OpenDialog()
        {
            if (service != null)
            {
                ServiceNameTextBox.Text = service.Name;
                ImageNameTextBox.Text = service.Image;
            }
            ShowDialog();
        }

        private void Save()
        {
            service = new Service();
            service.Series = new ObservableCollection<Series>();

            service.Name = ServiceName;
            service.Image = ImageName;

            DataManager.GetServiceList().services.Add(service);
        }

        private void Update()
        {
            DataManager dm = DataManager.GetInstance();
            service.Name = ServiceName;
            service.Image = ImageName;
        }

        private void OnSave(object sender, RoutedEventArgs e)
        {
            DataManager dm = DataManager.GetInstance();

            if (service == null)
                Save();
            else
                Update();

            DataManager.SortServices();
            dm.SaveServicesXML();

            Close();
		}
	}
}
