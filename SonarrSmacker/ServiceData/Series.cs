﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Collections;

namespace SonarrSmacker.ServiceData
{
	[XmlRoot("Series")]
	public class Series
	{
		[XmlAttribute("Name")]
		public string Name { get; set; }

		[XmlAttribute("SeriesId")]
		public int SeriesId { get; set; }

		public override string ToString()
		{
			StringBuilder rVal = new StringBuilder();
			rVal.AppendLine("----SERIES----");
			rVal.AppendLine("Name:  " + this.Name);
			rVal.AppendLine("SeriesId:  " + this.SeriesId);

			return rVal.ToString();
		}

	}

	public class SeriesComparer : IComparer
	{
		public int Compare(Object x, Object y)
		{
			return (new CaseInsensitiveComparer()).Compare((x as ServiceData.Series).Name, (y as ServiceData.Series).Name);
		}
	}
}
