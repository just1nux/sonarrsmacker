﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace SonarrSmacker.ServiceData
{
	[XmlRoot("ServiceList")]
	public class ServiceList
	{
		[XmlElement(ElementName = "Setting")]
		public Setting setting;

		//[XmlArray(ElementName = "Services")]
		//[XmlArrayItem(ElementName = "Service", Type = typeof(Service))]
        [XmlElement(ElementName = "Service")]
		public ObservableCollection<Service> services { get; set; }

		public override string ToString() {
			StringBuilder rVal = new StringBuilder();
			rVal.AppendLine("----SERVICES----");
			foreach (Service service in services)
			{
				rVal.Append(service.ToString());
			}

			return rVal.ToString();
		}

	}
}
