﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace SonarrSmacker.ServiceData
{
    public class DataManager
    {
        public static DataManager instance;
        private ServiceList serviceList = null;
        string XMLPATH = System.IO.Path.Combine(Directory.GetCurrentDirectory(), @"XML");
        string XMLFILE = "services.xml";

        //private constructor
        private DataManager() { }

        public static DataManager GetInstance()
        {
            if (instance == null)
            {
                instance = new DataManager();
                instance.LoadServicesXML(null, null);
                instance.StartXMLWatcher();
            }

            return instance;
        }

        public static ServiceList GetServiceList()
        {
            DataManager dm = DataManager.GetInstance();

            return dm.serviceList;
        }

        public void StartXMLWatcher()
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = XMLPATH;
            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
               | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            watcher.Filter = XMLFILE;

            watcher.Changed += new FileSystemEventHandler(LoadServicesXML);
            watcher.Created += new FileSystemEventHandler(LoadServicesXML);
            watcher.Deleted += new FileSystemEventHandler(LoadServicesXML);

            watcher.EnableRaisingEvents = true;
        }

        public void LoadServicesXML(object source, FileSystemEventArgs e)
        {
            Debug.WriteLine("LOAD SERVICES");
            ImportServicesXML();
            SortServices();

            if (LookupSeriesIds())
                SaveServicesXML();
		}

		public ServiceList ImportServicesXML()
		{
			string pathtoxml = System.IO.Path.Combine(XMLPATH, XMLFILE);

			using (System.IO.StreamReader xmlFile = GetReadStream(pathtoxml, 30000))
			{
				XmlSerializer serializer = new XmlSerializer(typeof(ServiceList));
				serviceList = serializer.Deserialize(xmlFile) as ServiceList;
			}

			return serviceList;
		}

		public void SaveServicesXML()
		{
			// Read the XML file.
			string pathtoxml = System.IO.Path.Combine(XMLPATH, XMLFILE);
			System.IO.StreamWriter xmlFile = new System.IO.StreamWriter(pathtoxml);

			// Serialize the data
			XmlSerializer serializer = new XmlSerializer(typeof(ServiceList));

			serializer.Serialize(xmlFile, serviceList);
			xmlFile.Close();
		}

		private StreamReader GetReadStream(string path, int timeoutMs)
		{
			var time = Stopwatch.StartNew();
			while (time.ElapsedMilliseconds < timeoutMs)
			{
				try {
					return new System.IO.StreamReader(path);
				} catch (IOException e) {
					if (e.HResult != -2147024864)
						throw;
				}
			}
			throw new TimeoutException($"Failed to get a handle to {path} within {timeoutMs}ms.");
		}

		public static bool SortServices()
		{
            Debug.WriteLine("SORTING");
            return GetServiceList().services.Sort(x => x.Name);

            //TODO: Sort list of services

            //IComparer serviceComparer = new ServiceComparer();
            //IComparer seriesComparer = new SeriesComparer();

            //Array.Sort(serviceList.services, serviceComparer);

            //foreach (Service service in serviceList.services)
            //	Array.Sort(service.Series, seriesComparer);
        }

		public bool LookupSeriesIds()
		{
            bool changed = false;
			foreach (Service service in serviceList.services)
			{
				foreach (Series series in service.Series)
				{
                    if (series.SeriesId < 1)
                    {
                        series.SeriesId = SonarrSmacker.SonarrRetriever.LookupSeriesId(series.Name);
                        changed = true;
                    }
				}
			}
            return changed;
		}
    }

    public static class ObservableCollection
    {
        public static bool Sort<TSource, TKey>(this ObservableCollection<TSource> source, Func<TSource, TKey> keySelector)
        {
            bool changed = false;
            List<TSource> sortedList = source.OrderBy(keySelector).ToList();
            if (sortedList.Equals(sortedList))
            {
                source.Clear();
                foreach (var sortedItem in sortedList)
                {
                    source.Add(sortedItem);
                }
                changed = true;
            }
            return changed;
        }
    }
}
