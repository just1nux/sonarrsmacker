﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SonarrSmacker.ServiceData
{
	[XmlRoot("Service")]
	public class Service 
	{
		[XmlAttribute("Name")]
		public string Name { get; set; }

		[XmlAttribute("Image")]
		public string Image { get; set; }

        [XmlElement(ElementName = "Series")]		
        public ObservableCollection<Series> Series{ get; set; }

		public override string ToString()
		{
			StringBuilder rVal = new StringBuilder();
			rVal.AppendLine("----SERVICE----" + Series.Count());
			rVal.AppendLine("Name:  " + Name);
			rVal.AppendLine("Image: " + Image);

			foreach (Series Series in Series)
			{
				rVal.Append(Series.ToString());
			}

			return rVal.ToString();
		}
	}

	public class ServiceComparer : IComparer
	{
		public int Compare(Object x, Object y)
		{
			return (new CaseInsensitiveComparer()).Compare((x as Service).Name, (y as Service).Name);
		}
	}
}
