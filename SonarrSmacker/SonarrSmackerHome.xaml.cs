﻿using SonarrSmacker.ServiceData;
using SonarrSmacker.ServiceWindows;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace SonarrSmacker
{

	public partial class SonarrSmackerHome : Page, ISonarrSmackerHome
	{
		EpisodeManager epm;

        public SonarrSmackerHome()
		{
			InitializeComponent();
			epm = new EpisodeManager();
            FillGeneral();
            ServicesComboBox.ItemsSource = DataManager.GetServiceList().services;
            ServiceListBox.ItemsSource = DataManager.GetServiceList().services;
            if (ServicesComboBox.SelectedIndex < 0)
                ServicesComboBox.SelectedIndex = 0;
            EpisodeManagerStartButton.IsEnabled = true;
            EpisodeManagerStopButton.IsEnabled = false;
            EpisodeManagerStatus.Text = "Status: Stopped";
        }

        public void Button_Click_Start(object sender, RoutedEventArgs e)
		{
			epm.StartUp();
            EpisodeManagerStartButton.IsEnabled = false;
            EpisodeManagerStopButton.IsEnabled = true;
            EpisodeManagerStatus.Text = "Status: Started";
		}

		public void Button_Click_Stop(object sender, RoutedEventArgs e)
		{
			epm.ShutDown();
            EpisodeManagerStartButton.IsEnabled = true;
            EpisodeManagerStopButton.IsEnabled = false;
            EpisodeManagerStatus.Text = "Status: Stopped";
        }

        public void Button_Click_Add_Service(object sender, RoutedEventArgs e)
		{
			PopUpAsk addServiceWindow = new PopUpAsk();
            addServiceWindow.ShowDialog();
		}

        public void Button_Click_Remove_Service(object sender, RoutedEventArgs e)
        {
            ConfirmRemoveService confirmRemoveServiceWindow = new ConfirmRemoveService();
            confirmRemoveServiceWindow.ShowDialog();

            if (confirmRemoveServiceWindow.result == 1) {
                Service service = (Service) ServiceListBox.SelectedItem;
                DataManager.GetServiceList().services.Remove(service);
            }
        }

        public void Button_Click_Edit_Service(object sender, RoutedEventArgs e)
        {
            PopUpAsk addServiceWindow = new PopUpAsk();
            addServiceWindow.service = (Service) ServiceListBox.SelectedItem;
            addServiceWindow.OpenDialog();
        }

        public void Button_Click_Add_Series(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("Add Series");
        }

        public void Button_Click_Remove_Series(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("Remove Series");
        }

        public void Button_Click_Edit_Series(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("Edit Series");
        }


        public void Save_Settings(object sender, RoutedEventArgs e)
        {
            DataManager dm = DataManager.GetInstance();
            Setting settings = DataManager.GetServiceList().setting;
            settings.SonarrURL = SonarrServerAddress.Text;
            settings.SonarrToken = SonarrServerToken.Text;
            settings.CheckEveryMinutes = Convert.ToInt32(CheckEveryMinutes.Text);
            dm.SaveServicesXML();
        }
        
        public void FillGeneral()
        {
            Setting settings = DataManager.GetServiceList().setting;       
            SonarrServerAddress.Text = settings.SonarrURL;
            SonarrServerToken.Text = settings.SonarrToken;
            CheckEveryMinutes.Text = settings.CheckEveryMinutes.ToString();
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ServicesTab.IsSelected)
            {
                ServicesComboBox.ItemsSource = DataManager.GetServiceList().services;
                if (ServicesComboBox.SelectedIndex < 0)
                    ServicesComboBox.SelectedIndex = 0;
            }
            if (SeriesTab.IsSelected)
            {
                ServiceListBox.ItemsSource = DataManager.GetServiceList().services;
            }
        }

        private void TextBox_TextChanged_NumbersOnly(object sender, TextChangedEventArgs e)
        {
            string s = Regex.Replace(((TextBox)sender).Text, @"[^\d]", "");
            ((TextBox)sender).Text = s;
        }

        public void ServiceComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e){
            ComboBox comboBox = (ComboBox)sender;
            Service selectedService = (Service) comboBox.SelectedItem;
            if (selectedService != null)
            {
                ObservableCollection<Series> series = selectedService.Series;
                SeriesListBox.ItemsSource = null;
                SeriesListBox.ItemsSource = series;
            }
        }
    }
}
