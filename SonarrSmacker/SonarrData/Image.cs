﻿using System.Runtime.Serialization;

namespace SonarrSmacker.SonarrData
{
	[DataContract]
	public class Image
	{
		[DataMember]
		public string coverType { get; set; }

		[DataMember]
		public string url { get; set; }
	}
}
