﻿using System.Runtime.Serialization;

namespace SonarrSmacker.SonarrData
{
	[DataContract]
	public class Season
	{
		[DataMember]
		public int seasonNumber { get; set; }
		[DataMember]
		public bool monitored { get; set; }
		[DataMember]
		public Statistics statistics { get; set; }
	}
}
