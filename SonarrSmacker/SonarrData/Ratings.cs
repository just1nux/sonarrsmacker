﻿using System.Runtime.Serialization;

namespace SonarrSmacker.SonarrData
{
	[DataContract]
	public class Ratings
	{
		[DataMember]
		public int votes { get; set; }
		[DataMember]
		public double value { get; set; }
	}
}
