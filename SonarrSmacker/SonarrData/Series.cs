﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SonarrSmacker.SonarrData
{
	[DataContract]
	public class Series
	{
		[DataMember]
		public string title { get; set; }
		[DataMember]
		public List<AlternateTitle> alternateTitles { get; set; }
		[DataMember]
		public string sortTitle { get; set; }
		[DataMember]
		public int seasonCount { get; set; }
		[DataMember]
		public int totalEpisodeCount { get; set; }
		[DataMember]
		public int episodeCount { get; set; }
		[DataMember]
		public int episodeFileCount { get; set; }
		[DataMember]
		public long sizeOnDisk { get; set; }
		[DataMember]
		public string status { get; set; }
		[DataMember]
		public string overview { get; set; }
		[DataMember]
		public string previousAiring { get; set; }
		[DataMember]
		public string network { get; set; }
		[DataMember]
		public string airTime { get; set; }
		[DataMember]
		public List<Image> images { get; set; }
		[DataMember]
		public List<Season> seasons { get; set; }
		[DataMember]
		public int year { get; set; }
		[DataMember]
		public string path { get; set; }
		[DataMember]
		public int profileId { get; set; }
		[DataMember]
		public bool seasonFolder { get; set; }
		[DataMember]
		public bool monitored { get; set; }
		[DataMember]
		public bool useSceneNumbering { get; set; }
		[DataMember]
		public int runtime { get; set; }
		[DataMember]
		public int tvdbId { get; set; }
		[DataMember]
		public int tvRageId { get; set; }
		[DataMember]
		public int tvMazeId { get; set; }
		[DataMember]
		public string firstAired { get; set; }
		[DataMember]
		public string lastInfoSync { get; set; }
		[DataMember]
		public string seriesType { get; set; }
		[DataMember]
		public string cleanTitle { get; set; }
		[DataMember]
		public string imdbId { get; set; }
		[DataMember]
		public string titleSlug { get; set; }
		[DataMember]
		public string certification { get; set; }
		[DataMember]
		public List<string> genres { get; set; }
		[DataMember]
		public List<object> tags { get; set; }
		[DataMember]
		public string added { get; set; }
		[DataMember]
		public Ratings ratings { get; set; }
		[DataMember]
		public int qualityProfileId { get; set; }
		[DataMember]
		public int id { get; set; }
	}

	[DataContract]
	public class AlternateTitle
	{
		[DataMember]
		public string title { get; set; }
		[DataMember]
		public int seasonNumber { get; set; }
	}

	[DataContract]
	public class Statistics
	{
		[DataMember]
		public string previousAiring { get; set; }
		[DataMember]
		public int episodeFileCount { get; set; }
		[DataMember]
		public int episodeCount { get; set; }
		[DataMember]
		public int totalEpisodeCount { get; set; }
		[DataMember]
		public object sizeOnDisk { get; set; }
		[DataMember]
		public int percentOfEpisodes { get; set; }
	}
}
