﻿using System.Runtime.Serialization;

namespace SonarrSmacker.SonarrData
{
	[DataContract]
	public class Record
	{
		[DataMember]
		public int seriesId { get; set; }
		[DataMember]
		public int episodeFileId { get; set; }
		[DataMember]
		public int seasonNumber { get; set; }
		[DataMember]
		public int episodeNumber { get; set; }
		[DataMember]
		public string title { get; set; }
		[DataMember]
		public string airDate { get; set; }
		[DataMember]
		public string airDateUtc { get; set; }
		[DataMember]
		public string overview { get; set; }
		[DataMember]
		public bool hasFile { get; set; }
		[DataMember]
		public bool monitored { get; set; }
		[DataMember]
		public bool unverifiedSceneNumbering { get; set; }
		[DataMember]
		public Series series { get; set; }
		[DataMember]
		public int id { get; set; }
	}
}
