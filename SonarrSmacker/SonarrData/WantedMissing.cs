﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SonarrSmacker.SonarrData
{
	[DataContract]
	public class WantedMissing
	{
		[DataMember]
		public int page { get; set; }
		[DataMember]
		public int pageSize { get; set; }
		[DataMember]
		public string sortKey { get; set; }
		[DataMember]
		public string sortDirection { get; set; }
		[DataMember]
		public int totalRecords { get; set; }
		[DataMember]
		public List<Record> records { get; set; }
	}
}
