﻿using SonarrSmacker.ServiceData;
using SonarrSmacker.SonarrData;
using System;
using System.Diagnostics;
using System.IO;
using System.Timers;

namespace SonarrSmacker
{
    public enum Status
    {
        STOPPED = -1,
        STARTED = 0,
        RUNNING = 1
    }

    public class EpisodeManager
    {
        public static Status status { get; set; } = Status.STOPPED;
        Timer t;

        public EpisodeManager()
        {
            t = new Timer(TimeSpan.FromMinutes(DataManager.GetServiceList().setting.CheckEveryMinutes).TotalMilliseconds);
            t.AutoReset = true;
            t.Elapsed += new System.Timers.ElapsedEventHandler(RunMissing);
            t.Enabled = true;
            t.Stop();
        }

        public void StartUp()
        {
            t.Start();
            status = Status.STARTED;
        }

        public void ShutDown()
        {
            t.Stop();
            status = Status.STOPPED;
        }

        public void RunMissing(object sender, ElapsedEventArgs e)
        {
            Status prevStatus = status;

            if (status != Status.RUNNING)
            {
                status = Status.RUNNING;
                Debug.WriteLine("Run Missing");

                WantedMissing wm = SonarrRetriever.RetreiveWantedMissing();
                foreach (Record record in wm.records)
                {
                    ServiceData.Service service = null;

                    if (record.series.qualityProfileId == 6)
                        service = GetServiceBySeriesId(record.seriesId);

                    if (service != null)
                    {
                        int startIndex = record.series.path.LastIndexOf('\\') + 1;
                        int endIndex = record.series.path.Length - 1;
                        int length = endIndex - startIndex + 1;
                        string safeName = record.series.path.Substring(startIndex, length);

                        string baseFileName = $"{safeName} - S{record.seasonNumber:00}E{record.episodeNumber:00}";
                        string mpgFileName = $"{baseFileName}.mpg";

                        string jpgFileName = $"{baseFileName}.jpg";
                        string pngFileName = $"{baseFileName}.png";

                        //check for jpg or png file
                        string sourceFileName = "";
                        string sourcePath = "";
                        string jpgSourcePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), $"Images\\{service.Image}.jpg");
                        string pngSourcePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), $"Images\\{service.Image}.png");

                        if (System.IO.File.Exists(jpgSourcePath))
                        {
                            sourceFileName = jpgFileName;
                            sourcePath = jpgSourcePath;
                        }
                        else if (System.IO.File.Exists(pngSourcePath))
                        {
                            sourceFileName = pngFileName;
                            sourcePath = pngSourcePath;
                        }
                        else
                        {
                            Debug.WriteLine("Image Not Found");
                        }

                        CopySourceImages(sourcePath, sourceFileName, record, mpgFileName);
                    }
                }
            }
            status = prevStatus;
        }

        public Service GetServiceBySeriesId(int SeriesId)
        {
            Service serviceVal = null;

            foreach (Service service in DataManager.GetServiceList().services)
            {
                foreach (ServiceData.Series series in service.Series)
                {
                    if (series.SeriesId == SeriesId)
                    {
                        serviceVal = service;
                        break;
                    }
                }
            }

            if (serviceVal == null)
            {
                Debug.WriteLine($"Service not found for SeriesId {SeriesId}");
            }
            return serviceVal;
        }

        private void CopySourceImages(string sourcePath, string sourceFileName, Record record, string mpgFileName)
        {
            if (sourcePath == null)
                return;
            if (sourceFileName == null)
                return;

            // set target path
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            string targetPath = $"{record.series.path}\\Season {record.seasonNumber}\\";

            //If the path doesnt exist, then create it
            if (!System.IO.Directory.Exists(targetPath))
                System.IO.Directory.CreateDirectory(targetPath);

            string destPath = System.IO.Path.Combine(targetPath, mpgFileName);
            System.IO.File.Copy(sourcePath, destPath, true);

            destPath = System.IO.Path.Combine(targetPath, sourceFileName);
            System.IO.File.Copy(sourcePath, destPath, true);

            //Trigger Rescan for Series
            SonarrRetriever.RescanSeries(record.seriesId);
        }
    }
}

