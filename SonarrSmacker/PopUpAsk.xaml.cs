﻿using SonarrSmacker.ServiceData;
using System.Windows;

namespace SonarrSmacker
{
	public partial class PopUpAsk : Window
	{
        public Service service { get; set; }

		public string ServiceName
		{
			get
			{
				if (ServiceNameTextBox == null) return string.Empty;
				    return ServiceNameTextBox.Text;
			}
		}

        public string ImageName
        {
            get
            {
                if (ImageNameTextBox == null) return string.Empty;
                return ImageNameTextBox.Text;
            }
        }

        public PopUpAsk()
		{
			InitializeComponent();
		}

        public void OpenDialog()
        {
            if (service != null)
            {
                ServiceNameTextBox.Text = service.Name;
                ImageNameTextBox.Text = service.Image;
            }
            ShowDialog();
        }

        private void Save()
        {
            Service[] services = DataManager.GetServiceList().services;
            Service[] t_services = new Service[services.Length + 1];
            services.CopyTo(t_services, 0);

            service = new Service();
            service.Series = new Series[0];

            service.Name = ServiceName;
            service.Image = ImageName;

            t_services[services.Length] = service;
            DataManager.GetServiceList().services = t_services;
        }

        private void Update()
        {
            service.Name = ServiceName;
            service.Image = ImageName;
        }

        private void OnSave(object sender, RoutedEventArgs e)
        {
            DataManager dm = DataManager.GetInstance();

            if (service == null)
                Save();
            else
                Update();

            dm.SortServices();
            dm.SaveServicesXML();
            Close();
		}
	}
}
